import requests
import sys

url = 'https://gitlab.com/api/v4/projects/29844398/pipelines'
headers = {'PRIVATE-TOKEN': sys.argv[1]}
r = requests.get(url, headers=headers)
resp = r.json()


for p in resp:
    if p['status'] not in ['running']:
        continue
    print('id: ' + str(p['id']))
    print('status:' + str(p['status']))
    r = requests.post(url=f"https://gitlab.com/api/v4/projects/29844398/pipelines/{str(p['id'])}/cancel",
                      headers=headers)
